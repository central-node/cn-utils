
module.exports = {
  routerAuth: require('./lib/router-auth'),
  configPassport: require('./lib/config-passport')
};
